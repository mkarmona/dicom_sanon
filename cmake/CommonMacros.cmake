include(CMakeParseArguments)
# define vars:  
#    DICOM_ANON_MODULE_${name}_NAME
#    DICOM_ANON_MODULE_${name}_DEPS
#    DICOM_ANON_MODULE_${name}_LINKS
#    DICOM_ANON_MODULE_${name}_INCLUDE_DIR
#
#    dicom_anon_add_module(<name> [DEPENDS <list of modules>] [LINKS <list of external libraries>] [PCH <header name>])
macro(dicom_anon_add_module)
    # setting internal vars for easier mangling
    cmake_parse_arguments( tkm "" "" "DEPENDS;LINKS;PCH;INCS;SRCS" ${ARGN} )
    list(GET tkm_UNPARSED_ARGUMENTS 0 _name)
    string(TOUPPER "${_name}" _module_name)
    set(_module_class ${_name})

    message( "${tkm_UNPARSED_ARGUMENTS} ${_module_name} ${_module_class}" )

    if( DICOM_ANON_BUILD_${_module_name} )
        set( DICOM_ANON_MODULE_${_module_name}_NAME "${_module_class}" CACHE INTERNAL "Name of the module ${_module_class}")
        set( DICOM_ANON_MODULE_${_module_name}_DEPS "${tkm_DEPENDS}" CACHE INTERNAL "List of deps for module ${_module_name}")
        set( DICOM_ANON_MODULE_${_module_name}_LINKS "${tkm_LINKS}" CACHE INTERNAL "List of ext links for module ${_module_name}")
        set( DICOM_ANON_MODULE_${_module_name}_INCLUDE_DIR "${CMAKE_CURRENT_SOURCE_DIR}" CACHE INTERNAL "List of include dirs for module ${_module_name}")

        # include dir from deps modules
        set( _inc_dir "" )
        set( _link_list "" )
        foreach( _el ${DICOM_ANON_MODULE_${_module_name}_DEPS} )
            set( _mod_class ${_el} )
            string( TOUPPER "${_el}" _mod_name )
            list( APPEND _inc_dir ${DICOM_ANON_MODULE_${_mod_name}_INCLUDE_DIR} )
        endforeach( )

        foreach( _el ${DICOM_ANON_MODULE_${_module_name}_DEPS}            )
            string( TOUPPER "${_el}" _mod_name )
            list( APPEND _link_list ${DICOM_ANON_MODULE_${_mod_name}_NAME} )
            list( APPEND _link_list ${DICOM_ANON_MODULE_${_mod_name}_LINKS} )
        endforeach( )
        list( APPEND _link_list ${DICOM_ANON_MODULE_${_module_name}_LINKS} )

        list( REMOVE_DUPLICATES _link_list )

        include_directories( ${_inc_dir} )
        add_library( ${_module_class} ${tkm_SRCS} ${tkm_INCS} )
        target_link_libraries( ${_module_class} ${_link_list} )

        set_target_properties( ${_module_class} PROPERTIES
            VERSION   "${DICOM_ANON_MAJOR_VERSION}.${DICOM_ANON_MINOR_VERSION}.${DICOM_ANON_BUILD_VERSION}"
            SOVERSION "${DICOM_ANON_MAJOR_VERSION}.${DICOM_ANON_MINOR_VERSION}"
        )

        if( tkm_PCH )
            add_precompiled_header( ${_module_class} ${tkm_PCH} FORCEINCLUDE )
        else( )
            message("no pch was defined")
        endif( )

        install( TARGETS ${_module_class} 
            RUNTIME DESTINATION ${DICOM_ANON_INSTALL_RUNTIME_DIR}
            LIBRARY DESTINATION ${DICOM_ANON_INSTALL_LIBRARY_DIR}
            ARCHIVE DESTINATION ${DICOM_ANON_INSTALL_ARCHIVE_DIR} )

        # TODO finish from here
        install( FILES ${tkm_INCS}
            DESTINATION "${DICOM_ANON_INSTALL_INCLUDE_DIR}/${_module_name}" )

        message("have:${DICOM_ANON_HAVE_${_module_name}} 
            name:${DICOM_ANON_MODULE_${_module_name}_NAME}
            deps:${DICOM_ANON_MODULE_${_module_name}_DEPS}
            links:${DICOM_ANON_MODULE_${_module_name}_LINKS}
            inc_dir:${DICOM_ANON_MODULE_${_module_name}_INCLUDE_DIR}
            _inc_dir:${_inc_dir}
            _link_list:${_link_list}
            ")

        list( APPEND _inc_dir ${DICOM_ANON_MODULE_${_module_name}_INCLUDE_DIR} )
        list( REMOVE_DUPLICATES _inc_dir )
        set( DICOM_ANON_MODULE_${_module_name}_INCLUDE_DIR "${_inc_dir}" CACHE INTERNAL "List of include dirs for module ${_module_name}")
    endif( )

        message( "have:${DICOM_ANON_HAVE_${_module_name}}" )
endmacro()

