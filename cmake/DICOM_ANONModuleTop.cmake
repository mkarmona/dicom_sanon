#set(DICOM_ANON_CONFIG_CMAKE_DIR "${CMAKE_SOURCE_DIR}/cmake")

configure_file(${DICOM_ANON_SOURCE_DIR}/cmake/DICOM_ANONConfig.cmake.in   ${DICOM_ANON_BINARY_DIR}/cmake/DICOM_ANONConfig.cmake @ONLY)

include(CMakeParseArguments)
include(GenerateExportHeader)
