# This file sets up include directories, link directories, and
# compiler settings for a project to use ToolKit.  It should not be
# included directly, but rather through the DICOM_ANON_USE_FILE setting
# obtained from ToolKitConfig.cmake.

if(DICOM_ANON_USE_FILE_INCLUDED)
  return()
endif()
set(DICOM_ANON_USE_FILE_INCLUDED 1)

# Update CMAKE_MODULE_PATH so includes work.
list(APPEND CMAKE_MODULE_PATH ${DICOM_ANON_CMAKE_DIR})

# Add preprocessor definitions needed to use TOOLKIT.
set_property(DIRECTORY APPEND PROPERTY COMPILE_DEFINITIONS ${DICOM_ANON_DEFINITIONS})

# Add include directories needed to use TOOLKIT.
include_directories(${DICOM_ANON_INCLUDE_DIRS})

# Add link directories needed to use TOOLKIT.
link_directories(${DICOM_ANON_LIBRARY_DIRS})
