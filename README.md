# DCMTK Extension

Just a simple dicom anonymizer (just erasing some DICOM fields) using and extending DCMTK. The command is called `dcmanonymify` and it sets all necessary tags by default. You should append those params too ` -gin -nb `. Using dcmodify and dcmtk as base code you could append all other dcmodify commands you need.

- dcmodify -ie -ea (0010,0010) -ea (0010,0020)
- ea (0010,0030) -ea (0020,000E) -ea (0020,000D)
- ea (0008,0080) -ea (0008,0081) -ea (0008,0050)
- ea (0008,0090) -ea (0008,1070) -ea (0008,1155)
- ea (0010,1000) -ea (0020,0010) -ea (0020,4000)

## Dependencies

You will need all dcmtk depedencies to proceed.

## Unix compilation

Just type `cd` into source dir and type `bash build_linux.sh`. You will find **dcmanonymify** inside install/bin folder.

## WIndows compilation

Due to lack of a native `patch` command and still not supported by cmake indeed, it automatically downloads from my own dcmtk patched version.

Just type `cd` into source dir and use `cmake-gui` or `cmake` and don't miss to configure `CMAKE_INSTALL_PREFIX` and `INSTALL` the project.

## Binary versions

- [Linux 64bits](https://bitbucket.org/mkarmona/dicom_sanon/downloads/dcmanonymify_bin_linux_64bits.tar.gz) binary