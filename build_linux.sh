#!/bin/bash

rm -Rf build
rm -Rf install
mkdir -p {build,install}
folder_prefix=$(pwd)
cd build

export CXXFLAGS=-fpermissive
cmake -DCMAKE_INSTALL_PREFIX=${folder_prefix}/install ../
make && make install
